<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductViewController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\UpdateProfileController;
use App\Http\Controllers\StripeController;
use App\Models\Product;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $products = Product::orderBy("created_at",'desc')->take(8)->get();
    return view('home',compact('products'));
});

Auth::routes();
Route::get("admin",function(){
    return redirect("dashboard");
});
// change password
Route::resource('change-password', ChangePasswordController::class);
// update profile
Route::get('/update-profile/{user}',  [UpdateProfileController::class, 'editProfile'])->name("profile.edit");
Route::patch('/update-profile/{user}',  [UpdateProfileController::class, 'updateProfile'])->name('profile.update');

// routes for make ordering front pages
Route::get('/checkout', [CheckoutController::class, 'checkout'])->name('cart.checkout');
Route::get('add-to-cart/{id}', [ProductViewController::class, 'addToCart'])->name('add.to.cart');
Route::patch('update-cart', [ProductViewController::class, 'update'])->name('update.cart');
Route::delete('remove-from-cart', [ProductViewController::class, 'remove'])->name('remove.from.cart');
Route::get('/home',function(){
    return redirect('/');
})->name('home');
// route for admin
Route::resource('product-order', App\Http\Controllers\ProductOrderController::class);
Route::resource('brand', App\Http\Controllers\ModelController::class);
Route::resource('dashboard', App\Http\Controllers\DashboardController::class);
Route::resource('product-list', ProductViewController::class);
Route::resource('products', App\Http\Controllers\ProductController::class);
Route::resource('cart', App\Http\Controllers\CartController::class);
Route::resource('contact-us', App\Http\Controllers\ContactController::class);
Route::resource('contacts', App\Http\Controllers\ContactBackEndController::class);
Route::resource('users', App\Http\Controllers\UserController::class);
   
Route::get('stripe', [StripeController::class, 'stripe']);
Route::post('stripe', [StripeController::class, 'stripePost'])->name('stripe.post');