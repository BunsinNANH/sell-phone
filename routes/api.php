<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("products",function(){
    $products = Product::all();
    $filePath = 'http://127.0.0.1:8000/storage/images/products/';
    $data = [];
    foreach ($products as $product) {
        $items =[
            'id' => $product->id,
            'name' => $product->name,
            'screen' => $product->screen,
            'color' => $product->color,
            'ram' => $product->ram,
            'price' => $product->price,
            'type' => $product->type,
            'hard_disk' => $product->hard_disk,
            'cpu' => $product->cpu,
            'image' => $filePath.$product->image,
            'created_at' => $product->created_at,
            'updated_at' => $product->updated_at,
            'model_id' => $product->model_id,
        ];
        array_push($data,$items);
    }
    return $data;
});

Route::get("users",function(){
    return User::all();
});

Route::get("users/{id}",function($id){
    return User::findOrFail($id);
});

Route::post('users', function(Request $request) {
    $validator = Validator::make($request, [
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'phone' => ['required'],
        'password' => ['required', 'string', 'min:8', 'confirmed'],
    ]);
    if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput($request->input());
    }
    return User::create([
        'name' => $request->name,
        'email' => $request->email,
        'phone' => $request->phone,
        'password' => Hash::make($request->password),
        'role_id' => 2,
    ]);
    // return User::create($request->all());
});
// Route::resource('user', UserController::class);