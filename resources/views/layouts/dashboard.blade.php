<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - Sell Computer Admin</title>
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="{{ asset("css/styles.css") }}" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="{{ url("dashboard") }}">Sell Computer</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group d-none">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
                    <button class="btn btn-primary" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ url("dashboard") }}">Profile</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Dashboard</div>
                            <a class="nav-link" href="{{ url("dashboard") }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt text-white"></i></div>
                                Dashboard
                            </a>
                            <div class="sb-sidenav-menu-heading">Users Management</div>
                            <a class="nav-link {{ Request::is("users") ? "active":null }}" href="{{ url("users") }}">
                                <i class="fas fa-user fa-fw text-white" aria-hidden="true"></i> Users
                            </a>
                            <div class="sb-sidenav-menu-heading">Products Management</div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon">
                                    <i class="fab fa-product-hunt text-white"></i>
                                </div>
                                Products
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link {{ Request::is("products") ? "active":null }}" href="{{ url("products") }}">
                                        <i class="fas fa-desktop fa-fw text-white" aria-hidden="true"></i> Products
                                    </a>
                                    {{-- <a class="nav-link " href="{{ url("brands") }}">
                                        <i class="fa fa-bold fa-fw text-white" aria-hidden="true"></i> Brands
                                    </a> --}}
                                    <a class="nav-link {{ Request::is("brands") ? "active": null }}" href="{{ url("brand") }}" >
                                        <div class="sb-nav-link-icon"><i class="fas fa-sort-amount-up-alt text-white"></i></div>
                                        Products Model
                                    </a>
                                    <a class="nav-link {{ Request::is("product-order") ? "active": null }}" 
                                        href="{{ url("product-order") }}">
                                        <div class="sb-nav-link-icon"><i class="fas fa-sort-alpha-up text-white"></i></div>
                                        Products Order
                                    </a>
                                </nav>
                            </div>
                            <a class="nav-link {{ Request::is("contacts") ? "active": null }}" href="{{ url("contacts") }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-address-book text-white"></i></div>
                                Contact Us
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        {{-- <div class="small">Logged in as:</div>
                        Start Bootstrap --}}
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4 mt-4">
                        @yield('dashboard')
                    </div>
                </main>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="{{ asset("js/scripts.js") }}"></script>
        <script src="{{ asset("js/datatables-simple-demo.js") }}"></script>
        <script>
            $(document).ready(function(){
                $(".alert").fadeTo(2000, 500).slideUp(500, function() {
                    $(".alert").slideUp(500);
                });
            })
        </script>
        @stack('script')
    </body>
</html>

