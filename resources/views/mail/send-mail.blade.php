<div class="container" style="margin:0 auto;justify-content:center;width:60%;border:1px solid #ccd;" >
    <div class="card-header" style="text-align:center;">
        <h5>User Contact Data</h5>
    </div>
    <div class="card-body" style="border-top:1px solid #ccd; ">
        <h3 style="background:#ccd; list-style:none; margin-top:0%; padding:10px; color:#fff; text-align:center;">User Contacting Info</h3>
        <table>
            <tr style="border:1px solid #ccd;padding:20px;">
                <td style="width:30%;padding-left:10px;">Username:</td>
                <td>{{ $data['username'] }}</td>
            </tr>
            <tr style="border:1px solid #ccd;padding:20px;">
                <td style="width:30%;padding-left:10px;">Email:</td>
                <td>{{ $data['email'] }}</td>
            </tr>
            <tr style="border:1px solid #ccd;padding:20px;">
                <td style="width:30%;padding-left:10px;">Subject:</td>
                <td>{{ $data['subject'] }}</td>
            </tr>
            <tr style="border:1px solid #ccd;padding:20px;">
                <td style="width:30%;padding-left:10px;">Message:</td>
                <td>{{ $data['message'] }}</td>
            </tr>
        </table>
    </div>
</div>
