@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Laravel - Change Password with Current Password Validation</div>
                @if(Session::has('success'))
                    <div class="alert alert-success"><em>{!! session('success') !!}</em>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times</span></button>
                    </div>
                @endif
                <div class="card-body">
                    <form method="POST" action="{{ route('change-password.store') }}">
                        @csrf
                        @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                        <div class="form-group">
                            <label for="password" class="col-form-label text-md-right">Current Password</label>
                            <input id="password" type="password" class="form-control" name="current_password"
                            placeholder="Current Password" autocomplete="current-password">
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-form-label text-md-right">New Password</label>
                            <input id="new_password" type="password" class="form-control" name="new_password"
                            placeholder="New Password" autocomplete="new-password">
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-form-label text-md-right">New Confirm Password</label>
                            <input id="new_confirm_password" type="password" class="form-control" 
                            placeholder="New Confirm Pssword" name="new_confirm_password" autocomplete="current-password">
                        </div>
                        <div class="form-group">
                            <a href="{{ url("/") }}" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-primary float-right">
                                Update Password
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
