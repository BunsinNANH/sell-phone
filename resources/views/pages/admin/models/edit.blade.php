@extends('layouts.dashboard')
@section('dashboard')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                @if ($message = Session::get('warning'))
                    <div class="alert alert-warning alert-block">
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Model</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route("brand.update",$model->id) }}" method="post">
                            @csrf
                            @method("PATCH")
                            <div class="form-group mb-4">
                                <label for="name">Model Name</label>
                                <input type="text" name="name" id="name" class="form-control" 
                                placeholder="Model Name" value="{{ $model->name }}">
                                @if ($errors->has('name'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <a href="{{ url("brand") }}" class="btn btn-secondary">Cancel</a>
                                <button type="submit" class="btn btn-success float-right">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection