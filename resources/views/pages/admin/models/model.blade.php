@extends('layouts.dashboard')
@section('dashboard')
    <div class="row justify-content-center">
        <div class="col-md-5">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <h4>Model Listing</h4>
        </div>
        <div class="col-md-2 mr-auto">
            <a href="{{ route("brand.create") }}" class="btn btn-sm btn-primary float-right">
                <i class="fa fa-plus-circle"></i> Add Model
            </a>
        </div>
    </div>
    <table class="table table-striped table-horvered table-bordered mt-3" id="datatablesSimple">
        <thead>
            <tr>
                <th>#</th>
                <th>Model Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($models as $model)
                <tr>
                    <td width="5%">{{ $index++ }}</td>
                    <td width="40%">
                        {{-- <img src="{{ asset("images/products/Samsung-Galaxy-s10plus.jpg") }}" alt="" width="5%"> --}}
                        <span>{{ $model->name }}</span>
                    </td>
                    <td width="10%">
                        {{-- <a href="#"><i class="fas fa-eye fa-fw text-primary"></i></a> --}}
                        <a href="#" data-toggle="modal" data-target="#delete-model"
                            data-id="{{ $model->id }}" data-name="{{ $model->name }}">
                            <i class="fas fa-trash-alt fa-fw text-danger"></i>
                        </a>
                        <a href="{{ route("brand.edit",$model->id) }}">
                            <i class="fas fa-pencil-alt fa-fw text-success"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="modal fade" id="delete-model" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" method="POST" id="form-delete-model" enctype="multipart/form-data">
                    @method('DELETE')
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title" id="deleteModelLabel">Delete Model</h4>
                    </div>
                    <div class="modal-body">
                        <p>Do you sure to <b>delete</b> model name <b><span class="text-danger" id="model-name"></span></b>?</p>
                        <div class="text-center">
                            {{-- <img src="" alt="" width="50%" id="product-image" class="text-center"> --}}
                        </div>
                    </div>
                    <div class="modal-footer text-white">
                        <button type="button" class="btn btn-link waves-effect btn-success left" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-link btn-danger waves-effect right">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function(){
            $('#delete-model').on('show.bs.modal',function (event){
                var button = $(event.relatedTarget)
                var id = button.data('id');
                var modelName = button.data('name');
                // var proImage = button.data('image');
                var modal = $(this);
                var url ='{{ route("brand.destroy", "id") }}';
                $('#model-name').text(modelName);
                $('#form-delete-model').attr('action',url.replace('id', id));
            });
        });
    </script>
@endpush