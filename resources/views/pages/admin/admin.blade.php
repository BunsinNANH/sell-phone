@extends('layouts.dashboard')
@section('dashboard')
    <h4 class="mt-4">Dashboard</h4>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <table class="table table-striped table-hovered table-bordered" id="datatablesSimple">
        <thead>
            <tr>
                <th>#</th>
                <th>Product Name</th>
                <th>Brand</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td width="5%">{{ $index++ }}</td>
                    <td width="40%">
                        <img src="{{ asset("/images/products/".$product->image) }}" 
                        alt="" width="5%">
                        <span>{{ $product->name }}</span>
                    </td>
                    <td width="15%">{{ $product->model->name }}</td>
                    <td width="15%">${{ $product->price }}.00</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
