@extends('layouts.dashboard')
@section('dashboard')
    <div class="row justify-content-center">
        <div class="col-md-5">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <h4>Product Orders Listing</h4>
        </div>
    </div>
    <table class="table table-striped table-horvered table-bordered mt-3" id="datatablesSimple">
        <thead>
            <tr>
                <th>#</th>
                <th>Customer Name</th>
                <th>Email</th>
                <th>Order ID</th>
                <th>Order Item</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
          @foreach ($orderItem as $item)
          <tr>
              <td width="5%">{{ $index++ }}</td>
              <td width="20%">
                  <span>{{ $item->order->user->name }}</span>
              </td>
              <td width="20%">
                  <span>{{ $item->order->user->email }}</span>
              </td>
              <td width="20%">
                  <span>{{ $item->order_id }}</span>
              </td>
              <td width="20%">
                <span>
                    {{ $item->product->name }} x 
                    {{ $item->quantity }}
                </span>
            </td>
              <td width="20%">
                  <span>
                      {{ $item->amount*$item->quantity }}
                </span>
              </td>
              <td width="20%">
                <span>{{ $item->status }}</span>
            </td>
              <td width="10%">
                  <a href="#">confirm</a>
                  <a href="#">reject</a>
              </td>
          </tr>
      @endforeach
        </tbody>
    </table>
@endsection