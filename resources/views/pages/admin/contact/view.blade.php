@extends('layouts.dashboard')
@section('dashboard')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h3>View Contact</h3><hr>
                <a href="{{ url("contacts") }}" class="btn btn-sm btn-warning">
                    <i class="fa fa-arrow-left"></i>Back
                </a>
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                              Username:
                            <span>{{ $contact->username }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                              Email:
                              <span>{{ $contact->email }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                              Subject:
                              <span>{{ $contact->subject }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                              Message:
                              <span>{{ $contact->message }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection