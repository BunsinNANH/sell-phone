@extends('layouts.dashboard')
@section('dashboard')
    <div class="row justify-content-center">
        <div class="col-md-5">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <h4>Contactor Listing</h4>
        </div>
    </div>
    <table class="table table-striped table-horvered table-bordered mt-3" id="datatablesSimple">
        <thead>
            <tr>
                <th>#</th>
                <th>Username</th>
                <th>Email</th>
                <th>Subject</th>
                <th>Message</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($contacts as $contact)
                <tr>
                    <td width="5%">{{ $index++ }}</td>
                    <td width="20%">
                        <span>{{ $contact->username }}</span>
                    </td>
                    <td width="20%">
                        <span>{{ $contact->email }}</span>
                    </td>
                    <td width="20%">
                        <span>{{ $contact->subject }}</span>
                    </td>
                    <td width="20%">
                        <span>{{ $contact->message }}</span>
                    </td>
                    <td width="10%">
                        <a href="{{ route("contacts.show",$contact->id) }}">
                            <i class="fas fa-eye fa-fw text-primary"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection