@extends('layouts.dashboard')
@section('dashboard')
    <div class="row justify-content-center">
        <div class="col-md-5">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('warning'))
                <div class="alert alert-warning alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <h4>Useres Listing</h4>
        </div>
    </div>
    <table class="table table-striped table-horvered table-bordered mt-3" id="datatablesSimple">
        <thead>
            <tr>
                <th>#</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td width="5%">{{ $index++ }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role->role }}</td>
                    <td class="text-center">
                        <a href="{{ route("users.show",$user->id) }}">
                            <i class="fas fa-eye fa-fw text-primary"></i>
                        </a>
                        <a href="#"  data-toggle="modal" data-target="#delete-user" 
                        data-id="{{ $user->id }}" data-name="{{ $user->name }}">
                            <i class="fas fa-trash-alt fa-fw text-danger"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="modal fade" id="delete-user" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" method="POST" id="form-delete-user" enctype="multipart/form-data">
                    @method('DELETE')
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title" id="deleteuserLabel">Delete User</h4>
                    </div>
                    <div class="modal-body">
                        <p>Do you sure to <b>delete</b> <b><span class="text-danger" id="user-name"></span></b> ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect text-white btn-success float-left" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-link btn-danger text-white waves-effect float-right">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function(){
            $('#delete-user').on('show.bs.modal',function (event){
                var button = $(event.relatedTarget)
                var id = button.data('id');
                var username = button.data('name');
                var modal = $(this);
                var url ='{{ route("users.destroy", "id") }}';
                $('#user-name').text(username);
                $('#form-delete-user').attr('action',url.replace('id', id));
            });
        });
    </script>
@endpush
