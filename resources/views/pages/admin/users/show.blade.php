@extends('layouts.dashboard')
@section('dashboard')
    <a href="{{ url("users") }}" class="btn btn-sm mb-3 btn-warning">
        <i class="fa fa-arrow-left"></i>
        Back
    </a>
    <div class="row justify-content-center">
        <div class="col-md-6 col-lg-6">
            <div class="card text-center">
                <div class="card-header">
                    <h3>User Detail</h3>
                </div>
                <div class="card-body">
                    <h1>{{ $user->name }}</h1>
                    <p><b>Email: </b>{{ $user->email }}</p>
                    <p><b>Role : </b>{{ $user->role->role }}</p>
                </div>
            </div>
        </div>
    </div>
@endsection