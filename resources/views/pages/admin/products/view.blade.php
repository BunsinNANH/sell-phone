@extends('layouts.dashboard')
@section('dashboard')
    <div class="container">
        <h3>Product Detail</h3><hr>
        <a href="{{ url("products") }}" class="btn btn-sm mb-3 btn-warning">
            <i class="fa fa-arrow-left"></i>
            Back
        </a>
        <div class="card mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <img src="{{ asset("/images/products/".$product->image) }}"  width="50%" >
                        <h3>{{ $product->name }}</h3>
                        <p>Model: <small id="model" class="text-warning">{{ $product->model->name }}</small></p>
                    </div>
                    <div class="col-md-6">
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                              RAM:
                            <span>{{ $product->ram }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                              Color:
                              <span>{{ $product->color }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                              Screen:
                              <span>{{ $product->screen }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                              CPU:
                              <span>{{ $product->cpu }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                              Storage:
                              <span>{{ $product->hard_disk }}</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                              Price:
                              <span>{{ $product->price }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection