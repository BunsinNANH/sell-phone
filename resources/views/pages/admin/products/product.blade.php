@extends('layouts.dashboard')
@section('dashboard')
    <div class="row justify-content-center">
        <div class="col-md-5">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <h4>Products Listing</h4>
        </div>
        <div class="col-md-2 mr-auto">
            <a href="{{ route("products.create") }}" class="btn btn-sm btn-primary float-right">
                <i class="fa fa-plus-circle"></i> Add Product
            </a>
        </div>
    </div>
    <table class="table table-striped table-horvered table-bordered mt-3" id="datatablesSimple">
        <thead>
            <tr>
                <th>#</th>
                <th>Product Name</th>
                <th>Brand</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
                <tr>
                    <td width="5%">{{ $index++ }}</td>
                    <td width="40%">
                        <img src="{{ asset("/images/products/".$product->image) }}" alt="" width="5%">
                        <span>{{ $product->name }}</span>
                    </td>
                    <td width="15%">{{ $product->model->name }}</td>
                    <td width="15%">${{ $product->price }}.00</td>
                    <td width="10%">
                        <a href="{{ route("products.show",$product->id) }}">
                            <i class="fas fa-eye fa-fw text-primary"></i>
                        </a>
                        <a href="#" data-toggle="modal" data-target="#delete-product" 
                            data-id="{{ $product->id }}" data-name="{{ $product->name }}"
                            data-model="{{ $product->model->name }}"
                            data-image="{{ asset("/images/products/".$product->image) }}">
                            <i class="fas fa-trash-alt fa-fw text-danger"></i>
                        </a>
                        <a href="{{ route("products.edit",$product->id) }}">
                            <i class="fas fa-pencil-alt fa-fw text-success"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="modal fade" id="delete-product" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" method="POST" id="form-delete-product" enctype="multipart/form-data">
                    @method('DELETE')
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title" id="deleteProductLabel">Delete Product</h4>
                    </div>
                    <div class="modal-body">
                        <p>Do you sure to <b>delete</b> <b><span class="text-danger" id="product-name"></span></b> ?</p>
                        <div class="text-center">
                            <img src="" alt="" width="50%" id="product-image" class="text-center">
                        </div>
                        <p>Model:<span id="model-name"></span></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect text-white btn-success float-left" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-link btn-danger text-white waves-effect float-right">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function(){
            $('#delete-product').on('show.bs.modal',function (event){
                var button = $(event.relatedTarget)
                var id = button.data('id');
                var proName = button.data('name');
                var modelName = button.data('model');
                var proImage = button.data('image');
                var modal = $(this);
                var url ='{{ route("products.destroy", "id") }}';
                $('#product-name').text(proName);
                $('#model-name').text(modelName);
                $('#product-image').attr('src',proImage);
                $('#form-delete-product').attr('action',url.replace('id', id));
            });
        });
    </script>
@endpush
