<style>
    #description{
        height: 60px;
    }
</style>
@extends('layouts.dashboard')
@section('dashboard')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                @if ($message = Session::get('warning'))
                    <div class="alert alert-warning alert-block">
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-1-">
                <div class="card">
                    <div class="card-header">
                        <h4>Create Product</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route("products.store") }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method("POST")
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group mt-2">
                                        <label for="name">Product Name</label>
                                        <input type="text" name="name" id="name" class="form-control"
                                        placeholder="Product Name">
                                        @if ($errors->has('name'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group mt-2">
                                        <label for="price">Price</label>
                                        <input type="text" name="price" id="price" class="form-control"
                                        placeholder="Price">
                                        @if ($errors->has('price'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group mt-2">
                                        <label for="image">Product Image</label>
                                        <input type="file" name="image" id="image" class="form-control"
                                        placeholder="Product Image">
                                        @if ($errors->has('image'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group mt-2">
                                        <label for="model">Product Model</label>
                                        <select name="model_id" id="model" class="form-control">
                                            <option disabled selected>Select Model</option>
                                            @foreach ($models as $model)
                                                <option value="{{ $model->id }}">{{ $model->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('model_id'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('model_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mt-2">
                                        <label for="color">Color</label>
                                        <input type="text" name="color" id="color" class="form-control"
                                        placeholder="Color">
                                        @if ($errors->has('color'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('color') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group mt-2">
                                        <label for="screen">Screen</label>
                                        <input type="text" name="screen" id="screen" class="form-control"
                                        placeholder="Screen">
                                        @if ($errors->has('screen'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('screen') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group mt-2">
                                        <label for="ram">RAM</label>
                                        <input type="text" name="ram" id="ram" class="form-control"
                                        placeholder="RAM">
                                        @if ($errors->has('ram'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('ram') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group mt-2">
                                        <label for="cpu">CPU</label>
                                        <input type="text" name="cpu" id="cpu" class="form-control"
                                        placeholder="CPU">
                                        @if ($errors->has('cpu'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('cpu') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group mt-2">
                                        <label for="storage">Storage</label>
                                        <input type="text" name="storage" id="storage" class="form-control"
                                        placeholder="Storage">
                                        @if ($errors->has('storage'))
                                            <span class="text-danger">
                                                <strong>{{ $errors->first('storage') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-4">
                                <a href="{{ url("products") }}" class="btn btn-secondary">Cancel</a>
                                <button type="submit" class="btn btn-success float-right">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection