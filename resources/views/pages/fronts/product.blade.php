@extends("layouts.app")
@section('content')
    <div class="banner-content">
        <img src="{{ asset("/images/banner/banner1.jpg") }}" alt="Image banner" width="100%">
    </div>
    <div class="container">
        <h2>Products Listing</h2>
        <hr>
        <div class="row mt-2">
            @foreach ($products as $product)
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <img src="{{ asset("/images/products/".$product->image) }}" alt="Product Image" width="60%">
                            <h5>{{ $product->name }}</h5>
                            <p><small>
                                <span class="text-secondary"><b>Model: </b></span> 
                                <span class="text-warning">{{ $product->model->name }}</span>
                            </small></p>
                            <p><b>Price:</b> <span class="text-danger">${{ $product->price }}</span> 
                                <a href="#" data-toggle="modal" data-target="#product-detail" 
                                    data-cpu="{{ $product->cpu }}" data-id="{{ $product->id }}"
                                    data-name="{{ $product->name }}" data-price="{{ $product->price }}"
                                    data-model="{{ $product->model->name }}" data-screen="{{ $product->screen }}"
                                    data-color="{{ $product->color }}" data-ram="{{ $product->ram }}"
                                    data-type="{{ $product->type }}" data-storage="{{ $product->storage }}"
                                    data-image="{{ asset("/images/products/".$product->image) }}"
                                    class="text-secondary">
                                    <small>More Detail</small>
                                </a>
                            </p>
                            <a href="{{ route('add.to.cart', $product->id) }}" 
                                class="btn btn-sm bg-warning form-control btn-checkout">
                                <i class="fa fa-plus-circle"></i> Add to cart
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="modal fade" id="product-detail" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <img src="" id="image" width="90%">
                            <h3 id="product-name"></h3>
                            <p>Model: <small id="model" class="text-warning"></small></p>
                        </div>
                        <div class="col-md-6">
                            <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                  RAM:
                                  <span id="ram"></span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                  Color:
                                  <span id="color"></span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                  Screen:
                                  <span id="screen"></span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                  CPU:
                                  <span id="cpu"></span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                  Storage:
                                  <span id="storage"></span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                  Price:
                                  <span id="price"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            $('#product-detail').on('show.bs.modal',function (event){
                var button = $(event.relatedTarget)
                var id = button.data('id');
                var name = button.data('name');
                var model = button.data('model');
                var proImage = button.data('image');
                var ram = button.data('ram');
                var color = button.data('color');
                var storage = button.data('storage');
                var screen = button.data('screen');
                var type = button.data('type');
                var price = button.data('price');
                var cpu = button.data('cpu');
                var modal = $(this);
                $('#product-name').text(name);
                $('#ram').text(ram);
                $('#color').text(color);
                $('#screen').text(screen);
                $('#type').text(type);
                $('#storage').text(storage);
                $('#cpu').text(cpu);
                $('#price').text("$"+price);
                $('#model').text(model);
                $('#image').attr('src',proImage);
            });
        });
    </script>
@endpush