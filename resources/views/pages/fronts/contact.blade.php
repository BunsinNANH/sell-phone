<style>
    textarea#message{
        height: 100px;
    }
</style>
@extends('layouts.app')
@section('content')
    <div class="banner-content">
        <img src="{{ asset("/images/banner/banner1.jpg") }}" alt="Image banner" width="100%">
    </div>
    <div class="container">
        <h4>Contact Us</h4>
        <hr>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route("contact-us.store") }}" method="POST">
                            @csrf
                            @method("POST")
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" name="username" id="username" placeholder="Username"
                                @if (Auth::check())
                                    value="{{ Auth::user()->name }}"
                                @endif
                                 class="form-control">
                                @if ($errors->has('username'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="email" id="email" placeholder="Email"
                                @if (Auth::check())
                                    value="{{ Auth::user()->email }}"
                                @endif class="form-control">
                                @if ($errors->has('email'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <input type="text" name="subject" id="subject" placeholder="Subject" class="form-control">
                                @if ($errors->has('subject'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea name="message" id="message" cols="10" rows="10" class="form-control"></textarea>
                                @if ($errors->has('message'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <button type="reset" class="btn btn-secondary float-left">Reset Form</button>
                            <button type="submit" class="btn btn-success float-right">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
