@extends('layouts.app')
@section("content")
    <div class="container">
        <table class="table table-striped table-horvered table-bordered" id="datatablesSimple">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @php $total = 0 @endphp
                @if(session('cart'))
                    @foreach(session('cart') as $id => $details)
                        @php $total += $details['price'] * $details['quantity'] @endphp
                        <tr data-id="{{ $id }}">
                            <td data-th="Product">
                                <div class="row">
                                    <div class="col-sm-3 hidden-xs"><img src="{{ asset("/images/products/".$details['image']) }}" width="60%" class="img-responsive"/></div>
                                    <div class="col-sm-9">
                                        <h6 class="nomargin">{{ $details['name'] }}</h6>
                                    </div>
                                </div>
                            </td>
                            <td data-th="Price">${{ $details['price'] }}</td>
                            <td data-th="Quantity">
                                <input type="number" value="{{ $details['quantity'] }}" class="form-control quantity update-cart" />
                            </td>
                            <td data-th="Subtotal" class="text-center">${{ $details['price'] * $details['quantity'] }}</td>
                            <td class="actions" data-th="">
                                <button class="btn btn-danger btn-sm remove-from-cart"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="6" class="text-right"><h4><strong>Total ${{ $total }}</strong></h4></td>
                </tr>
                <tr>
                    <td colspan="6" class="text-right">
                        <a href="{{ url('/') }}" class="btn btn-warning btn-sm">
                            <i class="fa fa-angle-left"></i> Continue Shopping
                        </a>
                        <a href="{{ url('stripe')}}" class="btn btn-sm btn-success">Checkout</a>
                        {{-- <a href="{{ route('cart.checkout')}}" class="btn btn-sm btn-success">Checkout</a> --}}
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
       $(".update-cart").change(function (e) {
            e.preventDefault();
            var ele = $(this);
            $.ajax({
                url: '{{ route('update.cart') }}',
                method: "patch",
                data: {
                    _token: '{{ csrf_token() }}', 
                    id: ele.parents("tr").attr("data-id"), 
                    quantity: ele.parents("tr").find(".quantity").val()
                },
                success: function (response) {
                    window.location.reload();
                }
            });
        });
        $(".remove-from-cart").click(function (e) {
            e.preventDefault();
            var ele = $(this);
            if(confirm("Are you sure want to remove?")) {
                $.ajax({
                    url: '{{ route('remove.from.cart') }}',
                    method: "DELETE",
                    data: {
                        _token: '{{ csrf_token() }}', 
                        id: ele.parents("tr").attr("data-id")
                    },
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });
    </script>
@endpush

