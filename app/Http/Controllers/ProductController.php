<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Brand;
use Validator;
use Illuminate\Support\Facades\Storage;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }
    public function index()
    {
        $index = 1;
        $products = Product::orderBy("created_at",'desc')->get();
        return view("pages.admin.products.product",compact("index","products"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $models = Brand::orderBy("name",'asc')->get();
        return view("pages.admin.products.add",compact('models'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'price' => 'required',
            'color' => 'required',
            'ram' => 'required',
            'cpu' => 'required',
            'screen' => 'required',
            'storage' => 'required',
            'image' => 'required',
            'model_id' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }

        if ($request->hasFile("image")) {
            $products = new Product();
            $products->name = $request->name;
            $products->price = $request->price;
            $products->color = $request->color;
            $products->ram = $request->ram;
            $products->cpu = $request->cpu;
            $products->storage = $request->storage;
            $products->screen = $request->screen;
            $products->model_id = $request->model_id;

            $path = '/images/products/';
            $proImage = $request->file("image")->getClientOriginalName();
             $request->file("image")->move(public_path($path), $proImage);
            // $request->file('image')->storeAs($path, $proImage);
            $products->image = $proImage;

            $products->save();
            return redirect('products')->with("success","Product created successfully!");
        }else{
            return redirect()->back()->with("error","Product fail created!");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view("pages.admin.products.view",compact("product"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $models = Brand::orderBy("name",'asc')->get();
        $product = Product::findOrFail($id);
        return view("pages.admin.products.edit",compact('product','models'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'price' => 'required',
            'color' => 'required',
            'ram' => 'required',
            'cpu' => 'required',
            'screen' => 'required',
            'storage' => 'required',
            'model_id' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        $product = Product::findOrFail($id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->color = $request->color;
        $product->ram = $request->ram;
        $product->cpu = $request->cpu;
        $product->screen = $request->screen;
        $product->storage = $request->storage;
        $product->model_id = $request->model_id;

        if ($request->hasFile("image")) {
            $path = '/images/products/';
            $proImage = $request->file("image")->getClientOriginalName();
            // $request->file('image')->storeAs($path, $proImage);
            $request->file("image")->move(public_path($path), $proImage);
            $product->image = $proImage;

            $product->save();
            return redirect('products')->with("success","Product updated successfully!");
        }else{
            $product->image = $product->image;
            $product->save();
            return redirect("products")->with("success","Product updated successfully !");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect()->back()->with('success','Product deleted successfully !');
    }
}
