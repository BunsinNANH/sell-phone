<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Stripe;
use Auth;
use App\Models\Order;
use App\Models\OrderItem;
class StripeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
        $cart = session()->get('cart');
        $totalAmount = 0;
        foreach ($cart as $item) {
            $totalAmount += $item['price'] * $item['quantity'];
        }
        return view('pages.fronts.stripe')->with('totalAmount', $totalAmount);
    }
   
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        $cart = session()->get('cart');
        $totalAmount = 0;
        $products = "";
        // dd($cart);
        foreach ($cart as $item) {
            $totalAmount += $item['price'] * $item['quantity'];
            $products .= $item['id']. ", "; 
        }

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $charge = Stripe\Charge::create([
            "amount" => $totalAmount * 100,
            "currency" => "usd",
            "source" => $request->stripeToken,
            "description" => "User: " . Auth::user()->id . " Product: " . $products
        ]);
        $response = $charge->jsonSerialize();
        if (empty($response["failure_code"]) && $response['paid'] == 1 && $response['status'] == 'succeeded') {

            $order = new Order();
            $order->user_id = Auth::user()->id;
            $order->amount = $totalAmount;
            $order->transaction_id = $response["balance_transaction"];
            $order->order_status = "succeeded";
            $order->card_cvc = $request->card_cvc;
            $order->card_expiry_month = $request->card_expiry_month;
            $order->card_expiry_year = $request->card_expiry_year;
            $order->card_holder_number = $request->card_holder_number;
            $order->card_holder_name = $request->card_holder_name;
            $order->save();
            $orderId = $order->id;
            foreach ($cart as $item) {
                $orderItem = new OrderItem();
                $orderItem->order_id = $order->id;
                $orderItem->product_id = $item['id'];
                $orderItem->quantity = $item['quantity'];
                $orderItem->amount = $item['price'];
                $orderItem->order_id = $orderId;
                $orderItem->save();
            }
            session()->put('cart', []);
            Session::flash('success', 'Payment successful!');
        }
        return redirect('/');
    }
}

