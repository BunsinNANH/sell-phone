<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table='orders';
    protected $primarykey='id';
    protected $fillable = [
        'amount','user_id',
        'transaction_id','card_cvc',
        'card_expiry_month',
        'card_expiry_year',
        'order_status',
        'card_holder_number',
        'card_holder_name'
    ];
        
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
