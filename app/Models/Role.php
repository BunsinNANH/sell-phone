<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table='roles';
    protected $primarykey='id';
    protected $fillable = [
        'role',
    ];

    // relationship one role have many users
    public function users(){
        return $this->hasMany(User::class);
    }

}
