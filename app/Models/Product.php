<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table='products';
    protected $primarykey='id';
    protected $fillable = [
        'name','screen','price','image','color','type','ram','hard_disk','cpu','model_id'
    ];

    // // relationship many products belongs to many users
    // public function users(){
    //     return $this->belongsToMany(User::class,'user_id');
    // }
    // relationship many products belongs to one brand
    public function model(){
        return $this->belongsTo(Brand::class,'model_id');
    }
    
    public function orders()
    {
        return $this->belongsToMany(Order::class,'order_items');
    }
}
