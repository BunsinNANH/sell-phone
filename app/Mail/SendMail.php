<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    public $username;
    public $email;
    public $subject;
    public $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username,$email,$subject,$message)
    {
        $this->username = $username;
        $this->email = $email;
        $this->subject = $subject;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'username'               =>         $this->username,
            'email'                  =>         $this->email,
            'subject'                =>         $this->subject,
            'message'                =>         $this->message,
        ];
        return $this->subject($this->subject)
                    ->view('mail.send-mail',compact("data"));
    }
}
