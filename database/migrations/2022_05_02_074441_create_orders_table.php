<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer("user_id")->unsiged();
            $table->decimal("amount");
            $table->string('transaction_id')->length(200)->nullable();
            $table->integer('card_cvc')->length(5)->nullable();
            $table->string('card_expiry_month',30)->nullable();
            $table->string('card_expiry_year',30)->nullable();
            $table->string('order_status',50)->nullable();
            $table->string('card_holder_number',250)->nullable();
            $table->string('card_holder_name',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
